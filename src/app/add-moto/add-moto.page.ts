import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { strict } from 'assert';

@Component({
  selector: 'app-add-moto',
  templateUrl: './add-moto.page.html',
  styleUrls: ['./add-moto.page.scss'],
})
export class AddMotoPage implements OnInit {

  marca: string;
  modelo: string;
  year: string;
  precio: string;
  foto: any;

  constructor(private router: Router) { }

  ngOnInit() {

  }


  postMoto() {
    this.foto = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];

    const form ={
      "marca" : this.marca,
      "modelo" : this.modelo,
      "year" : this.year,
      "foto" : this.foto,
      "precio" : this.precio,
  };
  
  
    const url = "http://enrique-moran-7e3.alwaysdata.net/miapi/moto";
    console.log(JSON.stringify(form));
    
    fetch(url, {
      "method": "POST",
      "mode": "cors",
      "headers": {
        "Accept": 'application/json',
        'Content-Type': 'application/json'
      },
      "body": JSON.stringify(form),

    })
    .then(response => {
      console.log("hola")
        this.router.navigateByUrl('/home');
      });
  }



}

