import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-destino',
  templateUrl: './destino.page.html',
  styleUrls: ['./destino.page.scss'],
})
export class DestinoPage implements OnInit {
  moto:any;

  constructor(private route: ActivatedRoute , private router: Router) { 

    this.route.queryParams.subscribe(params =>{

      if (this.router.getCurrentNavigation().extras.state){
        this.moto = this.router.getCurrentNavigation().extras.state.parametros;
      }
    })
  }

  ngOnInit() {
  }

  borrar() {
   
    const url = "http://enrique-moran-7e3.alwaysdata.net/miapi/deletemoto/" + this.moto.id;

    console.log(url);
    
    fetch(url, {
      "method": "DELETE",
      "mode": "cors",
    })
    .then(response => {

        this.router.navigate(['home']);
      });
  }


}
