import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController, ViewWillEnter } from '@ionic/angular';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit , ViewWillEnter {

  motos: any;
  test: any;

  constructor(private menu: MenuController , private router: Router) {
   
  }

  ngOnInit() {
    this.getJson();
  }
  
  ionViewWillEnter(){
    this.getJson();
  }
  
  async getJson(){
    const respuesta = await fetch("http://enrique-moran-7e3.alwaysdata.net/miapi/getmotos");
    this.motos= await respuesta.json();
    this.menu.close();

  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  detalle(moto){
    console.table(moto);
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: moto,
      }
    };
    this.router.navigate(['destino'], navigationExtras);
  }

  addMoto(){

    this.router.navigate(['add-moto']);

  }

  /*
  async getJsonDucati(){
    const respuesta = await fetch("http://motos.puigverd.org/motos?marca=ducati");
    this.motos= await respuesta.json();
    this.menu.close();
   
  }
  */

  async getJsonDucati(){
    const respuesta = await fetch("http://enrique-moran-7e3.alwaysdata.net/miapi/getducatis");
    this.motos= await respuesta.json();
    this.menu.close();
   
  }

  async getJsonHonda(){
    const respuesta = await fetch("http://enrique-moran-7e3.alwaysdata.net/miapi/gethondas");
    this.motos= await respuesta.json();
    this.menu.close();
   
  }

  async getJsonYamaha(){
    const respuesta = await fetch("http://enrique-moran-7e3.alwaysdata.net/miapi/getyamahas");
    this.motos= await respuesta.json();
    this.menu.close();
  }


}
