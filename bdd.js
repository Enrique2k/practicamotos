const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyparser = require('body-parser');
const { response } = require('express');
const { get } = require('http');
const multer = require('multer');
const app = express();
const baseUrl = '/miapi';
var upload = multer({ dest: 'foto/' })
app.use(cors());
app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json());
//La configuració de la meva bbdd
ddbbConfig = {
    user: 'enrique-moran-7e3',
    host: 'postgresql-enrique-moran-7e3.alwaysdata.net',
    database: 'enrique-moran-7e3_motos',
    password: 'ITB2019017',
    port: 5432
};
//El pool es un congunt de conexions
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/getmotos
const getMotos = (request, response) => {
   var consulta = "select * from motos"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
       response.status(200).json(results.rows)
       
   });
}

app.get(baseUrl + '/getmotos', getMotos);

const getDucatis = (request, response) => {
    var consulta = "select * from motos where lower(marca) = 'ducati'"
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
        response.status(200).json(results.rows)
   
    });
}

app.get(baseUrl + '/getducatis', getDucatis);

const getHondas = (request, response) => {
    var consulta = "select * from motos where lower(marca) = 'honda'"
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
        response.status(200).json(results.rows)
      
    });
}
 
app.get(baseUrl + '/gethondas', getHondas);

const getYamahas = (request, response) => {
    var consulta = "select * from motos where lower(marca) = 'yamaha'"
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
        response.status(200).json(results.rows)

    });
}

app.get(baseUrl + '/getyamahas', getYamahas);

const addMoto = (request, response) => {

    const { marca, modelo, year, foto, precio } = request.body;
   
    var consulta = "insert into motos(marca , modelo , year , foto , precio) VALUES('"+marca+ "','"+modelo+"','"+year+"','"+foto+"','"+precio+"')";

    pool.query(consulta, (error, results) => {
        if(error){
            throw error;
        }

    });

    response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body))
}

app.post(baseUrl + '/moto', addMoto);

const delMoto = (request, response) => {

    const id = request.params.id;

    var consulta = "delete from motos where id = "+id

    pool.query(consulta, (error, results) => {
        if(error){
            throw error;
        }
     
    });

    response.send('dd' + JSON.stringify(request.body))
}

app.delete(baseUrl + '/deletemoto/:id', delMoto);


//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
